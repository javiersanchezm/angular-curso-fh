import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pRiMeNg';

  /* nombre: string = 'JaVieR SaNcHez';
  valor: number = 753282323;
  obj = {
    nombre: 'Javier',
    años: 25
  }

  mostrar(){
    console.log(this.nombre, this.valor, this.obj);
  } */

  constructor(private primengConfig: PrimeNGConfig) {}

  ngOnInit() {
    this.primengConfig.ripple = true;
  }
}
