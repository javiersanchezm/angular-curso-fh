import { Component } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent {

  //i18nSelect
  nombre: string = 'Juan';
  genero: string = 'masculino';

  invitacionMapa = {
    'masculino': 'invitarlo',
    'femenino': 'invitarla'
  }

  //i18nPlural
  clientes:string[] = ['Juan', 'Pedro', 'Maria', 'Jesus', 'Eduardo'];

  clientesMapa = {
    '=0':'no tenemos ningún cliente esperando',
    '=1':'tenemos un cliente esperando',
    'other': 'tenemos # clientes esperando'
  }

  cambiarCliente(){
    if(this.genero == 'masculino'){
      this.nombre = 'Maria',
      this.genero = 'femenino'
    }
    else{
      this.nombre = 'Juan';
      this.genero = 'masculino';
    }
  }

  borrarCliente(){
    this.clientes.pop();
  }

  //KeyValue pipe
  persona = {
    nombre: 'Fernando',
    edad: 35,
    direccion: 'Ottawa, Canadá'
  }

  //JSON pipe
  heroes = [
    {
      nombre: 'Superman',
      vuela: true
    },
    {
      nombre: 'Robbin',
      vuela: false
    },
    {
      nombre: 'Aquaman',
      vuela: false
    }
  ]

  miObservable = interval(3000);

  valorPromesa = new Promise ( (resolve, reject) => {
    setTimeout(() => {
      resolve( 'Tenemos data de promesa');
    }, 3500);
  })
}
