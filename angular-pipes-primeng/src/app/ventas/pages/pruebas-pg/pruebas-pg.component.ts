import { Component } from '@angular/core';

@Component({
  selector: 'app-pruebas-pg',
  templateUrl: './pruebas-pg.component.html'
})
export class PruebasPgComponent {
  disabled: boolean = true;

  value1: string = '';

  value2: string = '';

  value3: string = '';

  value4: string = '';

  value5: string = 'Disabled';
}
