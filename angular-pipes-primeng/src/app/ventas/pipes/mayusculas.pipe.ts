import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mayusculas',
})
export class MayusculasPipe implements PipeTransform{

  transform( valor:string, enMayusculas: boolean = true ):string {

    //Uso del operador ternaro
    return (enMayusculas)? valor.toUpperCase() : valor.toLowerCase();
  }
}
