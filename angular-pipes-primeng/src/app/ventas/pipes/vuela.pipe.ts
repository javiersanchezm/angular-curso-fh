import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vuela'
})
export class VuelaPipe implements PipeTransform {

  transform( validar: boolean ): string {
    return (validar) ? 'si vuela' : 'no vuela';
  }

}
