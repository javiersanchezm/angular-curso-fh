import { Component, OnInit, OnChanges, DoCheck, AfterContentInit, AfterViewChecked, AfterContentChecked, OnDestroy, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-pagina1',
  templateUrl: './pagina1.component.html',
  styles: [
  ]
})

export class Pagina1Component implements OnInit, OnChanges,
DoCheck, AfterContentInit, AfterContentChecked, AfterViewChecked, OnDestroy{

    nombre: string = "Javier";

    segundos: number = 0;

    guardar(){
    }

    constructor(){
    console.log("constructor");
    }
    ngOnInit(): void {
    console.log('ngOnInit');
    }
    ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges');
    }
    ngDoCheck(): void {
    console.log('ngDoCheck');
    }
    ngAfterContentInit(): void {
    console.log('ngAfterContentInit');
    }
    ngAfterContentChecked(): void {
    console.log('ngAfterContentChecked');
    }
    ngAfterViewChecked(): void {
    console.log('ngAfterViewChecked');
    }
    ngOnDestroy(): void {
    console.log('ngOnDestroy');
    }


}
